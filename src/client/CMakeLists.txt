cmake_minimum_required(VERSION 3.5 FATAL_ERROR)
project(kovri-client CXX)

add_library(kovri-client "")
target_sources(kovri-client
  PUBLIC
    # API headers
    $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/instance.h>
    $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/tunnel_data.h>
    $<INSTALL_INTERFACE:include/kovri/client/instance.h>
    $<INSTALL_INTERFACE:include/kovri/client/tunnel_data.h>
  PRIVATE
    address_book/impl.cc
    address_book/storage.cc
    api/datagram.cc
    api/i2p_control/data.cc
    api/i2p_control/server.cc
    api/i2p_control/session.cc
    api/streaming.cc
    context.cc
    destination.cc
    instance.cc
    proxy/http.cc
    proxy/socks.cc
    reseed.cc
    service.cc
    tunnel.cc
    util/config.cc
    util/http.cc
    util/json.cc
    util/parse.cc
    util/uri/parser.cc
    util/zip.cc

    address_book/impl.h
    address_book/storage.h
    api/datagram.h
    api/i2p_control/data.h
    api/i2p_control/server.h
    api/i2p_control/session.h
    api/streaming.h
    context.h
    destination.h
    proxy/http.h
    proxy/socks.h
    reseed.h
    service.h
    tunnel.h
    util/config.h
    util/http.h
    util/json.h
    util/parse.h
    util/uri/buffer.h
    util/uri/error.h
    util/uri/parser.h
    util/uri/parts.h
    util/uri/rfc3986.h
    util/zip.h)

target_include_directories(kovri-client
  PUBLIC
    $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/..>
    $<INSTALL_INTERFACE:include>)

set_target_properties(kovri-client PROPERTIES
  PUBLIC_HEADER $<TARGET_PROPERTY:kovri-client,INTERFACE_SOURCES>)

target_link_libraries(kovri-client PUBLIC kovri-core kovri-internal)

if (COMMAND cotire)
  set_target_properties(kovri-client PROPERTIES COTIRE_PREFIX_HEADER_INCLUDE_PATH "${CMAKE_SOURCE_DIR}/deps")
  cotire(kovri-client)
endif()

add_library(kovri::kovri-client ALIAS kovri-client)
