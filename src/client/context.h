/**                                                                                           //
 * Copyright (c) 2015-2019, The Kovri I2P Router Project                                      //
 *                                                                                            //
 * All rights reserved.                                                                       //
 *                                                                                            //
 * Redistribution and use in source and binary forms, with or without modification, are       //
 * permitted provided that the following conditions are met:                                  //
 *                                                                                            //
 * 1. Redistributions of source code must retain the above copyright notice, this list of     //
 *    conditions and the following disclaimer.                                                //
 *                                                                                            //
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list     //
 *    of conditions and the following disclaimer in the documentation and/or other            //
 *    materials provided with the distribution.                                               //
 *                                                                                            //
 * 3. Neither the name of the copyright holder nor the names of its contributors may be       //
 *    used to endorse or promote products derived from this software without specific         //
 *    prior written permission.                                                               //
 *                                                                                            //
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY        //
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF    //
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL     //
 * THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,       //
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,               //
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS    //
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,          //
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF    //
 * THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.               //
 *                                                                                            //
 * Parts of the project are originally copyright (c) 2013-2015 The PurpleI2P Project          //
 */

#ifndef SRC_CLIENT_CONTEXT_H_
#define SRC_CLIENT_CONTEXT_H_

#include <map>
#include <memory>
#include <mutex>
#include <string>
#include <utility>

#include "client/address_book/impl.h"
#include "client/api/i2p_control/server.h"
#include "client/destination.h"
#include "client/proxy/http.h"
#include "client/proxy/socks.h"
#include "client/tunnel.h"

#include "core/util/exception.h"

namespace kovri
{
namespace client
{
/// @brief Class to manage context local destinations
/// @param t_destination Destination type, currently applicable to *client lib* local destinations
/// @notes The template could be useful for other types of future context (not just client) destination handling
template <typename t_destination>
class LocalDestinations final
{
 public:
  LocalDestinations() = default;
  ~LocalDestinations() = default;

  LocalDestinations(const LocalDestinations&) = delete;
  LocalDestinations& operator=(const LocalDestinations&) = delete;

  LocalDestinations(LocalDestinations&&) = delete;
  LocalDestinations& operator=(LocalDestinations&&) = delete;

  /// @brief Create and store a new local destination
  /// @param keys Keypair to be used for destination
  /// @param is_public Is this a public destination (for public lease-set, such as for server tunnel)
  /// @param params Destination parameters, currently I2CP parameters
  /// @notes Automatically starts the destination
  std::shared_ptr<t_destination> Create(
      const core::PrivateKeys& keys = core::PrivateKeys::CreateRandomKeys(),
      const bool is_public = false,
      const std::map<std::string, std::string>* params = nullptr);

  /// @brief Load and store a new local destination
  /// @param filename Filename (without path)
  /// @param is_public Is this a public destination (for public lease-set, such as for server tunnel)
  /// @notes Automatically starts the destination
  std::shared_ptr<t_destination> LoadFromFile(
      const std::string& filename,
      const bool is_public = false);

  /// @brief Find local destination
  std::shared_ptr<t_destination> Find(
      std::shared_ptr<t_destination> destination);

  /// @brief Find local destination by public hash
  std::shared_ptr<t_destination> Find(const core::IdentHash& ident);

  /// @brief Delete local destination
  /// @return True if deleted
  /// @note Also stops destination
  bool Delete(std::shared_ptr<t_destination> destination);

  /// @brief Delete local destination by public hash
  /// @return True if deleted
  /// @note Also stops destination
  bool Delete(const core::IdentHash& ident);

  /// @brief Start *all* object destinations
  void Start();

  /// @brief Stop *all* object destinations
  void Stop();

 private:
  std::mutex m_Mutex;
  std::map<core::IdentHash, std::shared_ptr<t_destination>> m_Destinations;
};

class ClientContext final
{
 public:
  ClientContext();
  ~ClientContext() = default;

  ClientContext(const ClientContext&) = delete;
  ClientContext& operator=(const ClientContext&) = delete;

  ClientContext(ClientContext&&) = delete;
  ClientContext& operator=(ClientContext&&) = delete;

  void Start();
  void Stop();

  /**
   * Shuts down the ClientContext and calls the shutdown handler.
   * This member function can be used by client components to shut down the
   *  router.
   * @note nothing happens if there is no registered shutdown handler
   * @warning not thread safe
   */
  void RequestShutdown();

  std::shared_ptr<ClientDestination> GetSharedLocalDestination() const {
    return m_SharedLocalDestination;
  }

  AddressBook& GetAddressBook() {
    return m_AddressBook;
  }

  const AddressBook& GetAddressBook() const {
    return m_AddressBook;
  }

  /// @brief Removes all server unnels satisfying the given predicate
  /// @param predicate a unary predicate used to filter server tunnels
  void RemoveServerTunnels(
      std::function<bool(I2PServerTunnel*)> predicate);

  /// @brief Removes all client tunnels satisfying the given predicate
  /// @param predicate a unary predicate used to filter client tunnels
  void RemoveClientTunnels(
      std::function<bool(I2PClientTunnel*)> predicate);

  /// @brief Removes all client/server tunnels by name
  /// @param client Vector of client tunnel names to remove
  /// @param server Vector of server tunnel names to remove
  void RemoveTunnels(
      const std::vector<std::string>& client,
      const std::vector<std::string>& server);

  /// @brief Updates or creates the specified server tunnel
  /// @param tunnel Const reference to populated/initialized tunnel attributes class
  /// @param http true if server tunnel is an HTTP tunnel
  void UpdateServerTunnel(
    const TunnelAttributes& tunnel,
    bool is_http);  // TODO(anonimal): this isn't ideal

  /// @brief Updates or creates the specified client tunnel
  /// @param tunnel Const reference to populated/initialized tunnel attributes class
  void UpdateClientTunnel(
    const TunnelAttributes& tunnel);

  /// @brief Creates the specified server tunnel and tries to insert it
  /// @param tunnel Const reference to populated/initialized tunnel attributes class
  /// @param http true if server tunnel is an HTTP tunnel
  /// @return true if the tunnel was inserted, false otherwise
  bool AddServerTunnel(
      const TunnelAttributes& tunnel,
      bool is_http);  // TODO(anonimal): this isn't ideal

  /// @brief Creates the specified client tunnel and tries to insert it
  /// @param tunnel Const reference to populated/initialized tunnel attributes class
  /// @return true if the tunnel was inserted, false otherwise
  bool AddClientTunnel(const TunnelAttributes& tunnel);

  /// @brief Registers a shutdown handler, called by ClientContext::RequestShutdown.
  /// @param handler The handler to be called on shutdown
  void RegisterShutdownHandler(std::function<void(void)> handler);

  /// @brief Inserts a client tunnel.
  /// @return true if the tunnel was inserted, false otherwise
  bool InsertClientTunnel(
      int port,
      std::unique_ptr<I2PClientTunnel> tunnel);

  /// @brief Inserts a server tunnel.
  /// @return true if the tunnel was inserted, false otherwise
  bool InsertServerTunnel(
      const kovri::core::IdentHash& id,
      std::unique_ptr<I2PServerTunnel> tunnel);

  /// @brief Sets the I2PControl service
  /// @param service a pointer to the I2PControlService
  void SetI2PControlService(
      std::unique_ptr<kovri::client::I2PControlService> service);

  /// @brief Sets the HTTP proxy.
  /// @param proxy a pointer to the HTTPProxy
  void SetHTTPProxy(std::unique_ptr<HTTPProxy> proxy);

  /// @brief Sets the SOCKS proxy.
  /// @param proxy a pointer to the SOCKSProxy
  void SetSOCKSProxy(std::unique_ptr<kovri::client::SOCKSProxy> proxy);

  /// @return the client tunnel with the given name, or nullptr
  I2PServerTunnel* GetServerTunnel(const std::string& name);

  /// @return the server tunnel with the given identity hash, or nullptr
  I2PServerTunnel* GetServerTunnel(const kovri::core::IdentHash& id);

  /// @return the client tunnel with the given name, or nullptr
  I2PClientTunnel* GetClientTunnel(const std::string& name);

  /// @return the client tunnel with the given (local) port
  I2PClientTunnel* GetClientTunnel(int port);

  boost::asio::io_service& GetIoService();

  /// @return Reference to local I2P destinations for tunnel usage
  auto& LocalDestinations()
  {
      return m_LocalDestinations;
  }

 private:
  client::LocalDestinations<client::ClientDestination> m_LocalDestinations;

  // Private (not published)
  std::shared_ptr<client::ClientDestination> m_SharedLocalDestination;

  AddressBook m_AddressBook;

  std::unique_ptr<HTTPProxy> m_HttpProxy;
  std::unique_ptr<kovri::client::SOCKSProxy> m_SocksProxy;

  std::mutex m_ClientMutex;
  // port->tunnel
  std::map<int, std::unique_ptr<I2PClientTunnel>> m_ClientTunnels;

  std::mutex m_ServerMutex;
  // destination->tunnel
  std::map<kovri::core::IdentHash, std::unique_ptr<I2PServerTunnel>> m_ServerTunnels;


  // types for accessing client / server tunnel map entries
  typedef std::pair<const int,
                    std::unique_ptr<I2PClientTunnel>> ClientTunnelEntry;

  typedef std::pair<const kovri::core::IdentHash,
                    std::unique_ptr<I2PServerTunnel>> ServerTunnelEntry;

  boost::asio::io_service m_Service;
  std::unique_ptr<I2PControlService> m_I2PControlService;

  std::function<void(void)> m_ShutdownHandler;

  kovri::core::Exception m_Exception;
};

extern ClientContext context;

}  // namespace client
}  // namespace kovri

#endif  // SRC_CLIENT_CONTEXT_H_
